import React, {useRef} from "react";
import {Carousel, Nav, Navbar} from "react-bootstrap";

export const App: React.FC = () => {
    const top = useRef(null)
    const catalog = useRef(null)
    const scrollToTop = () => top.current.scrollIntoView({behavior: 'smooth', block: 'end'})
    const scrollToCatalog = () => catalog.current.scrollIntoView({behavior: 'smooth', block: 'end'})

    return <div className="App">
        <Navbar bg="light" expand="lg" sticky="top">
            <Navbar.Brand className="jj_head" onClick={scrollToTop}>Julia Jewelry</Navbar.Brand>
            <Nav.Link className="jj_menu_item" onClick={scrollToCatalog}>Home</Nav.Link>
            <Nav.Link className="jj_menu_item">Link</Nav.Link>
        </Navbar>
        <div>
            <div ref={top}>
                <Carousel>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src="/img/slide1.jpg"
                            width="800" height="400"
                            alt="First slide"
                        />
                        <Carousel.Caption>
                            <h3>First slide label</h3>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src="/img/slide2.jpg"
                            width="800" height="400"
                            alt="Third slide"
                        />

                        <Carousel.Caption>
                            <h3>Second slide label</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src="/img/slide3.jpg"
                            width="800" height="400"
                            alt="Third slide"
                        />

                        <Carousel.Caption>
                            <h3>Third slide label</h3>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </div>

            <div ref={catalog}>
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
                test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            </div>
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
            test artetsc st stsd sdf a test artetsc st stsd sdf a test artetsc st stsd sdf a
        </div>
    </div>
};
